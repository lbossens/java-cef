#include "response_filter.h"
#include "jni_util.h"

ResponseFilter::ResponseFilter(JNIEnv* env, jobject handler)
    : handle_(env, handler) {}

bool ResponseFilter::InitFilter() {
  ScopedJNIEnv env;
  if (!env)
    return false;

  jboolean jresult = JNI_FALSE;

  JNI_CALL_METHOD(env, handle_, "initFilter", "()Z", Boolean, jresult);

  return (jresult != JNI_FALSE);
}

cef_response_filter_status_t ResponseFilter::Filter(void* data_in,
                                                    size_t data_in_size,
                                                    size_t& data_in_read,
                                                    void* data_out,
                                                    size_t data_out_size,
                                                    size_t& data_out_written) {
  ScopedJNIEnv env;
  if (!env)
    return RESPONSE_FILTER_ERROR;

  ScopedJNIIntRef jbytesRead(env, (int)data_in_read);
  jbyteArray jbytesIn = env->NewByteArray((jsize)data_in_size);
  env->SetByteArrayRegion(jbytesIn, 0, (jsize)data_in_size, (jbyte*)data_in);

  ScopedJNIIntRef jbytesWritten(env, (int)data_out_written);
  jbyteArray jbytesOut = env->NewByteArray((jsize)data_out_size);

  ScopedJNIObjectResult jresult(env);

  JNI_CALL_METHOD(env, handle_, "filter",
                  "([BLorg/cef/misc/IntRef;[BLorg/cef/misc/IntRef;)Lorg/cef/"
                  "handler/CefResponseFilter$FilterStatus;",
                  Object, jresult, jbytesIn, jbytesRead.get(), jbytesOut,
                  jbytesWritten.get());

  data_in_read = jbytesRead;
  data_out_written = jbytesWritten;

  jbyte* jbyte = env->GetByteArrayElements(jbytesOut, nullptr);
  if (jbyte) {
    memmove(data_out, jbyte, data_out_written);
    env->ReleaseByteArrayElements(jbytesOut, jbyte, JNI_ABORT);
  }
  env->DeleteLocalRef(jbytesOut);

  env->DeleteLocalRef(jbytesIn);

  if (IsJNIEnumValue(env, jresult,
                     "org/cef/handler/CefResponseFilter$FilterStatus",
                     "NeedMoreData")) {
    return RESPONSE_FILTER_NEED_MORE_DATA;
  } else if (IsJNIEnumValue(env, jresult,
                            "org/cef/handler/CefResponseFilter$FilterStatus",
                            "Done")) {
    return RESPONSE_FILTER_DONE;
  } else {  // Error
    return RESPONSE_FILTER_ERROR;
  }
}