package tests.detailed.handler;

import org.cef.handler.CefResponseFilter;
import org.cef.misc.IntRef;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StoreBodyResponseFilter implements CefResponseFilter {
    private final List<byte[]> bodyChunks = new ArrayList<>();

    @Override
    public boolean initFilter() {
        // nothing to initialize
        return true; // installs the filter
    }

    @Override
    public FilterStatus filter(
            byte[] dataIn, IntRef dataInRead, byte[] dataOut, IntRef dataOutWritten) {
        int nbBytesCopied = Math.min(dataIn.length, dataOut.length);
        dataInRead.set(nbBytesCopied);
        dataOutWritten.set(nbBytesCopied);

        if (nbBytesCopied > 0) {
            System.arraycopy(dataIn, 0, dataOut, 0, nbBytesCopied); // pass-through: copy in to out.
            bodyChunks.add(
                    Arrays.copyOf(dataIn, nbBytesCopied)); // Store the current chunk of the body.

            return FilterStatus.NeedMoreData;
        } else {
            return FilterStatus.Done;
        }
    }

    /**
     * Reconstructs the full body by concatenating all the chunks previously saved.
     * @return the body response.
     */
    public byte[] getResponseBodyAsBytes() {
        int bodySize = 0;
        for (byte[] b : bodyChunks) {
            bodySize += b.length;
        }

        byte[] responseBody = new byte[bodySize];
        int position = 0;
        for (byte[] b : bodyChunks) {
            System.arraycopy(b, 0, responseBody, position, b.length);
            position += b.length;
        }
        return responseBody;
    }

    public String getResponseBodyAsString() {
        return new String(getResponseBodyAsBytes()); // in this simplified example, we don't take
                                                     // the char encoding into account. We assume
                                                     // it's the default charset of the JVM.
    }
}
