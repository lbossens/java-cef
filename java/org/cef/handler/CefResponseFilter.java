package org.cef.handler;

import org.cef.misc.IntRef;

public interface CefResponseFilter {
    enum FilterStatus { NeedMoreData, Done, Error }
    /**
     * Initialize the response filter. Will only be called a single time.
     * The filter will not be installed if this method returns false.
     *
     * @return The filter will not be installed if this method returns false.
     */
    boolean initFilter();

    /**
     * Called to filter a chunk of data.
     * This method will be called repeatedly until there is no more data to filter (resource
     * response is complete), dataInRead matches dataIn.Length (all available pre-filter bytes have
     * been read), and the method returns FilterStatus.Done or FilterStatus.Error.<br> Remark: Do
     * not keep a reference to the buffers passed to this method.
     *
     * @param dataIn The input buffer containing pre-filter data. Can be null.
     * @param dataInRead Set to the number of bytes that were read from dataIn
     * @param dataOut output buffer that can accept filtered output data.
     *                Check dataOut.length for maximum buffer size.
     * @param dataOutWritten Set to the number of bytes that were written into dataOut
     * @return If some or all of the pre-filter data was read successfully but more data is needed
     *         in order
     * to continue filtering (filtered output is pending) return FilterStatus.NeedMoreData. If some
     * or all of the pre-filter data was read successfully and all available filtered output has
     * been written return FilterStatus.Done. If an error occurs during filtering return
     * FilterStatus.Error.
     */
    FilterStatus filter(byte[] dataIn, IntRef dataInRead, byte[] dataOut, IntRef dataOutWritten);
}
